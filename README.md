# SQL - Chantier Participatif

## Diagramme :

![EEDiagram](./diagramme.png)

## Contexte

Nous souhaiterions créer une application qui permettra de mettre en relation des hôtes·ses et des volontaires pour des activités de type WOOFING ou des chantiers participatifs.

## Fonctionnalités

Chaque utilisateur·ice aura un compte à son nom et pourra se déclarer hôte·sse, ce qui lui demandera de renseigner les informations liées à son lieu de vie 
(type de lieu, d'activités, adresse, capacité d'accueil etc.)

Les hôte·sse auront également la possibilité de créer des chantiers à des dates spécifiques, pour un nombre de personne donné et d'y inscrire les types d'activités qui seront pratiquées lors de ce chantier.

Les volontaires pourront consulter la liste des hôtes·ses ainsi que celle des chantiers à venir et soit faire une demande de "WOOFING" à un·e hôte·sse, soit directement s'inscire sur un chantier existant.

Un·e hôte·sse pourra valider la présences des différent·e·s volontaires le jour du chantier.

Le fait de participer à des chantiers fait monter le niveau de compétence du/de la volontaire liée aux activités pratiquées.

Un·e utilisateur·ice peut consulter dans son profil la liste des chantiers et des WOOFing effectués (ou non, si absent·e). Ielle peut également voir son niveau de compétence dans les différentes activités selon ses participation passées.

## Travail demandé

* Concevoir la base de données pour cette application
    * Créer un diagramme type merise des différentes tables et de leurs relations
    * Créer un script SQL de création des tables
    * Créer un script de mise en place de données de test
* Créer des procédures stockées pour les accès et les ajouts de données pertinentes
* Créer des trigger pour certaines des opérations
* Optionnel : Créer des composant d'accès au données dans le langage souhaité, avec ORM ou non (l'idée étant de faire le lien entre les procèdures stockées et le langage choisi)