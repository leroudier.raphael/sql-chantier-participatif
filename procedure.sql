USE `worksite`;

DROP procedure IF EXISTS `worksite`.`new_user`;

;

DELIMITER $ $ USE `worksite` $ $ CREATE DEFINER = `root` @`%` PROCEDURE `new_user`(
    IN i_mail VARCHAR(255),
    IN i_firstName VARCHAR(255),
    IN i_lastName VARCHAR(255),
    IN i_password VARCHAR(255),
    IN i_pseudo VARCHAR(255)
) BEGIN
SET
    i_password = SHA2(i_password, 512);

INSERT INTO
    `worksite`.`user` (mail, firstName, lastName, password, pseudo)
VALUES
    (
        i_mail,
        i_firstName,
        i_lastName,
        i_password,
        i_pseudo
    );

END $ $ DELIMITER;

;

USE `worksite`;

DROP procedure IF EXISTS `worksite`.`new_dockyard`;

;

DELIMITER $ $ USE `worksite` $ $ CREATE DEFINER = `root` @`%` PROCEDURE `new_dockyard`(
    IN i_owner_id INT,
    IN i_title VARCHAR(255),
    IN i_about TEXT,
    IN i_opening DATE,
    IN i_ending DATE,
    IN i_requiredMax TINYINT(4)
) BEGIN
INSERT INTO
    `worksite`.`dockyard` (owner_id, title, about, begining, ending, requiredMax)
VALUES
    (
        i_owner_id,
        i_title,
        i_about,
        i_begining,
        i_ending,
        i_requiredMax
    );

END $ $ DELIMITER;

;