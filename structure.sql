-- Creation de la base de donnees "worksite".
DROP DATABASE IF EXISTS `worksite`;

CREATE DATABASE IF NOT EXISTS `worksite` CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Creation de la table "user".
CREATE TABLE `worksite`.`user` (
    id INT auto_increment PRIMARY KEY,
    mail VARCHAR(255) NOT NULL,
    firstName VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    pseudo VARCHAR(255) NULL,
    progressList_id INT NULL,
    CONSTRAINT user_mail_UNIQ UNIQUE KEY (mail),
    CONSTRAINT user_pseudo_UNIQ UNIQUE KEY (pseudo)
) charset = utf8;

-- Creation de la table "activity".
CREATE TABLE `worksite`.`activity` (
    id INT auto_increment PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    CONSTRAINT activity_title_UNIQ UNIQUE KEY (title)
) charset = utf8;

-- Creation de la table "activity_list".
CREATE TABLE `worksite`.`activityList` (
    id INT NOT NULL PRIMARY KEY,
    activity_id INT NOT NULL,
    CONSTRAINT activity_list_UNIQ UNIQUE key (id, activity_id)
) charset = utf8;

-- Creation de la table "progress_list".
CREATE TABLE `worksite`.`progressList` (
    id INT NOT NULL PRIMARY KEY,
    activity_id INT NOT NULL,
    progress TINYINT NOT NULL
) charset = utf8;

-- Creation de la table "volunteer_list".
CREATE TABLE `worksite`.`volunteerList` (
    id INT NOT NULL PRIMARY KEY,
    user_id INT NOT NULL,
    beggining DATE NOT NULL,
    ending DATE NOT NULL,
    presence TINYINT default -1 NOT NULL,
    accepted TINYINT(1) NULL,
    CONSTRAINT volunteer_list_UNIQ UNIQUE KEY (id, user_id)
) charset = utf8;

-- Creation de la table "address".
CREATE TABLE `worksite`.`address` (
    id INT auto_increment PRIMARY KEY,
    streetNumber VARCHAR(30) NOT NULL,
    streetName VARCHAR(255) NOT NULL,
    infosMore VARCHAR(255) NULL,
    city_name VARCHAR(255) NOT NULL,
    zip_code VARCHAR(30) NOT NULL,
    state_name VARCHAR(150) NULL,
    country_name VARCHAR(150) NOT NULL
) charset = utf8;

-- Creation de la table "dockyard".
CREATE TABLE `worksite`.`dockyard` (
    id INT auto_increment PRIMARY KEY,
    owner_id INT NOT NULL,
    volunteerList_id INT NULL,
    activitiesList_id INT NULL,
    address_id INT NULL,
    title VARCHAR(255) NOT NULL,
    about TEXT NOT NULL,
    opening DATE NOT NULL,
    closing DATE NOT NULL,
    requiredMax TINYINT NOT NULL
) charset = utf8;

-- Création des Foreign Keys.
ALTER TABLE
    `worksite`.`user`
ADD
    CONSTRAINT progress_list_id_FK FOREIGN KEY (progressList_id) REFERENCES `worksite`.progressList(id) ON DELETE
SET
    null ON UPDATE cascade;

ALTER TABLE
    `worksite`.`activityList`
ADD
    CONSTRAINT activity_id_FK1 FOREIGN KEY (activity_id) REFERENCES `worksite`.activity(id);

ALTER TABLE
    `worksite`.`progressList`
ADD
    CONSTRAINT activity_id_FK2 FOREIGN KEY (activity_id) REFERENCES `worksite`.activity(id);

ALTER TABLE
    `worksite`.`volunteerList`
ADD
    CONSTRAINT user_id_FK FOREIGN KEY (user_id) REFERENCES `worksite`.user(id);

ALTER TABLE
    `worksite`.`dockyard`
ADD
    CONSTRAINT owner_id_FK FOREIGN KEY (owner_id) REFERENCES `worksite`.user(id) ON UPDATE cascade ON DELETE cascade;

ALTER TABLE
    `worksite`.`dockyard`
ADD
    CONSTRAINT address_id_FK FOREIGN KEY (address_id) REFERENCES `worksite`.address(id) ON UPDATE cascade ON DELETE cascade;

ALTER TABLE
    `worksite`.`dockyard`
ADD
    CONSTRAINT volunteer_list_FK FOREIGN KEY (volunteerList_id) REFERENCES `worksite`.volunteerList(id) ON DELETE
SET
    null ON UPDATE cascade;

ALTER TABLE
    `worksite`.`dockyard`
ADD
    CONSTRAINT activities_list_id_FK FOREIGN KEY (activitiesList_id) REFERENCES `worksite`.activityList(id);