-- Ajout des utilisateurs
CALL `worksite`.new_user('bob.smith@domain.ltd','Bob','Smith','pwd1','bob.smith');
CALL `worksite`.new_user('claudia.greer@domain.ltd','Claudia','Greer','pwd2','claudia.greer');
CALL `worksite`.new_user('marty.jones@domain.ltd','Marty','Jones','pwd3','marty.jones');
CALL `worksite`.new_user('marc.hernandez@domain.ltd','Marc','Hernandez','pwd4','marc.hernandez');
CALL `worksite`.new_user('teresa.murphy@domain.ltd','Teresa','Murphy','pwd5','teresa.murphy');
CALL `worksite`.new_user('elisabeth.de-la-vergne@domain.ltd','Élisabeth','De La Vergne','pwd6','elisabeth.de-la-vergne');
CALL `worksite`.new_user('philippine.jobin@domain.ltd','Philippine','Jobin','pwd7','philippine.jobin');
CALL `worksite`.new_user('arnaud.blanc@domain.ltd','Arnaud','Blanc','owd8','arnaud.blanc');

-- Création des chantier
CALL worksite.new_dockyard(1, 'Rénovation du chateau de Bélinay', 'Nous soliciton votre aide afin de refaire la toiture ainsi que les parquets du deuxième étage', '2021-01-01', '2021-02-01', 6);
CALL worksite.new_dockyard(2, 'Rénovation du chateau de Bélinay', 'Nous soliciton votre aide afin de refaire la toiture ainsi que les parquets du deuxième étage', '2021-01-01', '2021-02-01', 6);
